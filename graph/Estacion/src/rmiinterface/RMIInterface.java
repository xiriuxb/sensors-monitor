package rmiinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {

    public byte[] raltProm(byte[] a, byte[] b) throws RemoteException;
    public double[] gpsProm(double lat1, double long1, double lat2, double long2) throws RemoteException;
}