package estacion;


import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

import rmiinterface.RMIInterface;

public class Estacion extends UnicastRemoteObject implements RMIInterface{

    private static final long serialVersionUID = 1L;
    private static byte[] keyValue=new byte[] {'0','2','3','4','5','6','7','8','9','1','2','3','4','5','6','7'};

    
    protected Estacion() throws RemoteException {

        super();

    }
    
    public static String decrypt(byte[] encryptedText) throws Exception 
    {
            // generate key 
            Key key = new SecretKeySpec(keyValue, "AES");
            Cipher chiper = Cipher.getInstance("AES");
            chiper.init(Cipher.DECRYPT_MODE, key);
            //byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedText);
            byte[] decValue = chiper.doFinal(encryptedText);
            String decryptedValue = new String(decValue);
            return decryptedValue;
    }

    private static byte[] encrypt (String value){
        byte[] cipherText = null;
        try {
            Cipher cipher = Cipher.getInstance("AES");
                Key key = new SecretKeySpec(keyValue, "AES");; // get / create symmetric encryption key
                cipher.init(Cipher.ENCRYPT_MODE, key);
                byte[] plainText  =value.getBytes("UTF-8");
                cipherText = cipher.doFinal(plainText);
                
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Estacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Estacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Estacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Estacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(Estacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Estacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cipherText;
    }
    
    public byte[] raltProm(byte[] a, byte[] b) throws RemoteException{
    	double first=0, second=0;
		try {
			first = Double.parseDouble(decrypt(a));
			second = Double.parseDouble(decrypt(b));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    FileWriter fichero = null;
        try
        {

            fichero = new FileWriter("C://Users//HP//WebstormProjects//graph//Estacion//archivo2.txt");
            BufferedWriter buffer = new BufferedWriter(fichero);

            buffer.write(((first+second)/2)+"");
            buffer.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
        System.err.println(a + " ; ---"+b);
        return encrypt((first+second)/2 + "");

    }

        public double[] gpsProm(double lat1, double long1, double lat2, double long2) throws RemoteException{
            System.err.println("GPS Data: {"+lat1 + ","+long1+"};{"+lat2 + ","+long2+"}");
            double latProm = (lat1+lat2)/2;
            double longProm = (long1+long2)/2;
            double[] response = {latProm,longProm};
            return response;
        }

    public static void main(String[] args){

        try {
        	System.setProperty( "java.rmi.server.hostname", "192.168.56.1" ) ;
        	LocateRegistry.createRegistry(1099); 
            //Naming.rebind("rmi://"+InetAddress.getLocalHost().getHostAddress()+":1099/MyServer", new Estacion());
            Naming.rebind("rmi://192.168.56.1:1099/MyServer", new Estacion());
            System.err.println("Server ready");

        } catch (Exception e) {

            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();

        }

    }

}