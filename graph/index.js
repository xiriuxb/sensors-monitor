var express = require('express');
var cors = require('cors');
var app = express();
let value=0

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function logArrayElements() {
    let fs = require('fs');
    let archivo = fs.readFileSync('./Estacion/archivo2.txt', 'utf-8');
    return archivo;
}
app.use(cors());
app.get('/', function (req, res) {
    logArrayElements();
    res.send({data: logArrayElements() , code: 200, error: false });
});
app.listen(4000,'0.0.0.0', function () {
    console.log('API inicializada en el puerto 3000');
});