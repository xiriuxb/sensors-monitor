/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmiinterface;

/**
 *
 * @author jorge
 */
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote {

    public byte[] raltProm(byte[] a, byte[] b) throws RemoteException;
    public double[] gpsProm(double lat1, double long1, double lat2, double long2) throws RemoteException;

}