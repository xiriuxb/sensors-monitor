/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otro;

import java.text.DecimalFormat;
import otro.Avion;
import otro.Sensores;
import javax.swing.JLabel;

/**
 *
 * @author Jorge
 */
public class Hilo1 extends Thread{
    JLabel label;
    private static DecimalFormat df2 = new DecimalFormat("#.####");
    public Hilo1(JLabel label){
        this.label = label;
    }
    
    public void run(){
         try {
            
            while(1!=0){
                double auxGPS[] = Sensores.getDataGPS();
                double response[] = Avion.getGPS(auxGPS[0],auxGPS[1],auxGPS[2],auxGPS[3]);
                System.out.println("{"+auxGPS[0]+","+auxGPS[1]+","+auxGPS[2]+","+auxGPS[3]);
                label.setText(df2.format(response[0])+","+df2.format(response[1]));
                sleep(2000);
            }
                

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
