/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otro;

/**
 *
 * @author jorge
 */
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import rmiinterface.RMIInterface;
import vista.Principal;

public class Avion {

	private static RMIInterface look_up;
    private static byte[] keyValue=new byte[] {'0','2','3','4','5','6','7','8','9','1','2','3','4','5','6','7'};
    
            
        public static String decrypt(byte[] encryptedText) throws Exception 
    {
            // generate key 
            Key key = new SecretKeySpec(keyValue, "AES");
            Cipher chiper = Cipher.getInstance("AES");
            chiper.init(Cipher.DECRYPT_MODE, key);
            //byte[] decordedValue = new .decodeBuffer(encryptedText);
            byte[] decValue = chiper.doFinal(encryptedText);
            String decryptedValue = new String(decValue);
            return decryptedValue;
    }
	public static double getRalt(byte[] a, byte[] b) 
		throws MalformedURLException, RemoteException, NotBoundException, Exception {
            LocateRegistry.getRegistry(Principal.ipCentral, 1099);
		look_up = (RMIInterface) Naming.lookup("rmi://"+Principal.ipCentral+":1099/MyServer");	
		double response = Double.parseDouble(decrypt(look_up.raltProm(a,b)));
		return response;
	}
        
        public static double[] getGPS(double a, double b, double c, double d) 
		throws MalformedURLException, RemoteException, NotBoundException {
            LocateRegistry.getRegistry(Principal.ipCentral, 1099);
		look_up = (RMIInterface) Naming.lookup("rmi://"+Principal.ipCentral+":1099/MyServer");	
		double response[] = look_up.gpsProm(a,b,c,d);
		return response;
	}

}
