/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otro;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import otro.Avion;
import otro.Sensores;
import javax.swing.JLabel;

/**
 *
 * @author Jorge
 */
public class Hilo extends Thread {
    JLabel label,label1;
    private static byte[] keyValue=new byte[] {'0','2','3','4','5','6','7','8','9','1','2','3','4','5','6','7'};
    
    private byte[] encrypt (String textToEncrypt){
        byte[] cipherText = null;
        try {
            Cipher cipher = Cipher.getInstance("AES");
                Key key = new SecretKeySpec(keyValue, "AES");; // get / create symmetric encryption key
                cipher.init(Cipher.ENCRYPT_MODE, key);
                byte[] plainText  = textToEncrypt.getBytes("UTF-8");
                cipherText = cipher.doFinal(plainText);
                
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cipherText;
    }
    
    
    private static DecimalFormat df2 = new DecimalFormat("#.####");
    public Hilo(JLabel label,JLabel label1){
        this.label = label;
        this.label1 = label1;
    }
    
    public void run(){
         try {
            
            while(1!=0){
                double auxRalt[] = Sensores.getDataRalt();
                System.out.println(""+auxRalt[0]);
                label.setText(""+df2.format(Avion.getRalt(encrypt(auxRalt[0]+""),encrypt(auxRalt[1]+""))));
                if(auxRalt[2]!=0){
                    label1.setText("Error en Ralt Sensor "+ auxRalt[2]);
                }
                sleep(2000);
            }
                

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
