/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otro;

/**
 *
 * @author Jorge
 */
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import javax.swing.JOptionPane;

import rmiinterface.RMIInterfaceS;
import vista.Principal;
public class Sensores {
    private static rmiinterface.RMIInterfaceS look_up;

	public static double[] getDataRalt() 
		throws MalformedURLException, RemoteException, NotBoundException {
        LocateRegistry.getRegistry("192.168.56.101", 10001);
		look_up =  (RMIInterfaceS) Naming.lookup("rmi://"+Principal.ipSensors+":10001/My2Server");	
		double[] response = look_up.sendDataRalt();
		return response;
	}
        public static double[] getDataGPS() 
		throws MalformedURLException, RemoteException, NotBoundException {
        LocateRegistry.getRegistry("192.168.56.101", 10001);
		look_up =  (RMIInterfaceS) Naming.lookup("rmi://"+Principal.ipSensors+":10001/My2Server");	
		double[] response = look_up.sendDataGPS();
		return response;
	}
}
