# Sensors Monitor

Proyecto realizado con RMI, node y Angular para la Materia de Computación 
Distribuida de la Carrera de Ingeniería de Sistemas de la Escuela Politécnica Nacional. Simplemente como aprendizaje, por lo cual contiene bastantes errores y una programación muy sucia.

## Problema inicial:
En un avion se tienen 3 sensores de altitud y 3 de GPS, dos de estos estan activos, mientras uno de cada uno es de respaldo.
La computadora del avion obtiene los datos de los dos sensores funcionales y los envía a la estación central, en la estación se
calcula el promedio de estos dos sensores, y es este promedio el que se muestra en la cabina del avión.
Si existe un error de más de 5% entre los valores de los sensores, se desactiva el sensor que falla y se activa el de backup 
automáticamente.

## Solución inicial implementada
En la solución dada existen tres nodos. Los Sensores, la computadora del Avión y la Estación.
Los Sensores y la Estación son servidores RMI:
- En los sensores se implementan las funciones getDataRalt (que retorna los valores de los dos sensores activos de altura mas un valor para saber si
falla uno de estos sensores), y, getDataGPS(que retorna los valores de los dos sensores activos de GPS mas un valor para saber si
falla uno de estos sensores). Los sensores de GPS envían el dato de latitud y longitud por separado. Es en los sensores donde se controla el error (Funcionalidad no implementada en su totalidad).

- En la estación se implementan las funciones getRalt y getGPS, las cuales retornan el promedio de altura y GPS (lat, long), respectivamente.

La computadora del avión es cliente de estos Servidores RMI. De los sensores obtiene los datos y de la estación recibe el promedio de los valores.

## Mejora

Para la segunda entrega se implementó en la Estación un servidor en Node y una aplicación en Angular para que grafique los datos del promedio de la
altura. A demás de una encriptación en RSA de los datos enviados desde la computadora del avión hacia la Estación y viceversa (implementado solo para los datos de la altura).

## Ejecución
- La aplicación está diseniada para que se ejecute en por lo menos dos computadores, debido a que ejecutar dos servidores RMI en un mismo computador puede resultar en errores.
Se recomienda ejecutar los sensores y el avion enun computador y la estación en otra, o ejecutar cada nodo en un computador.
- Primero hay que ejecutar la Estación, luego los Sensores y finalmente el Avión.

## Notas
- Los datos de los sensores se obtienen de archivos de texto los cuales se pueden encontrar en el apartado de los Sensores.
- Las IP del Front de la app Web debe ser cambiada manualmente.
