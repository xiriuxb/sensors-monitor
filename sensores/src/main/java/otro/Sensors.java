/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otro;

/**
 *
 * @author Jorge
 */
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import rmiinterface.RMIInterfaceS;
import vista.Principal;


public class Sensors extends UnicastRemoteObject implements RMIInterfaceS{
    public static double[] dataRalt = new double[3];
    public static double[] dataGPS = new double[5];

    private static final long serialVersionUID = 1L;

    protected Sensors() throws RemoteException {
        super();
    }

    @Override
    public double[] sendDataRalt() throws RemoteException{
        System.out.println(Sensors.dataRalt[0] +" ; "+Sensors.dataRalt[1]);
        return dataRalt;
    }
    
    @Override
    public double[] sendDataGPS() throws RemoteException{
        return dataGPS;
    }

    public static void startServer(){        
        try {
            System.setProperty("java.rmi.server.hostname", Principal.ip);
            LocateRegistry.createRegistry(10001);
            Naming.rebind("rmi://"+Principal.ip+":10001/My2Server", new Sensors());
            System.err.println("Server ready");
        } catch (MalformedURLException ex) {
            Logger.getLogger(Sensors.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RemoteException ex) {
            Logger.getLogger(Sensors.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}