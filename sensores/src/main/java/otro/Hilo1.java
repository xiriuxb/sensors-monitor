/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otro;

import java.io.File;
import static java.lang.Thread.sleep;
import java.text.DecimalFormat;
import java.util.Scanner;
import javax.swing.JLabel;

/**
 *
 * @author Jorge
 */
public class Hilo1 extends Thread {
    private static DecimalFormat df2 = new DecimalFormat("#.####");
    JLabel label, label1;
    public Hilo1(JLabel label, JLabel label1){
        this.label = label;
        this.label1 = label1;
    }
    
    public void run(){
         try {
            Scanner input = new Scanner(new File("/home/osboxes/Documents/WindowsVM/sensores/src/lat1.txt"));
            Scanner input1 = new Scanner(new File("/home/osboxes/Documents/WindowsVM/sensores/src/long1.txt"));
            Scanner input2 = new Scanner(new File("/home/osboxes/Documents/WindowsVM/sensores/src/lat2.txt"));
            Scanner input3 = new Scanner(new File("/home/osboxes/Documents/WindowsVM/sensores/src/long2.txt"));
            while (input.hasNextLine()) {
                String line = input.nextLine();
                String line2 = input1.nextLine();
                String line3 = input2.nextLine();
                String line4 = input3.nextLine();
                Sensors.dataGPS[0]= Double.parseDouble(line);
                Sensors.dataGPS[1]= Double.parseDouble(line2);
                Sensors.dataGPS[2]= Double.parseDouble(line3);
                Sensors.dataGPS[3]= Double.parseDouble(line4);
                label.setText(line+","+line2);
                label1.setText(line3+","+line4);
                System.out.println(Sensors.dataGPS[0] +" ; "+Sensors.dataGPS[1]);
                sleep(200);
            }
            input.close();
            input1.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
