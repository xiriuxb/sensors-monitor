/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmiinterface;

/**
 *
 * @author Jorge
 */
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterfaceS extends Remote {

    public double[] sendDataRalt() throws RemoteException;
    public double[] sendDataGPS() throws RemoteException;

}
